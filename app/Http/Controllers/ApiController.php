<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User; 

class ApiController extends Controller
{
    public function getData(Request $request)
    {
        $usuarioSO = $request->input('UsuarioSO');
        $password = $request->input('Password');

        // Autenticación básica
        if (Auth::attempt(['username' => $usuarioSO, 'password' => $password])) {
            // Autenticación exitosa, ahora puedes acceder a los datos de la tabla

            // Se utiliza Eloquent para obtener todos los datos
            $datos = User::all();

            return response()->json(['mensaje' => 'Autenticación exitosa', 'datos' => $datos]);
        }

        return response()->json(['mensaje' => 'Error de autenticación'], 401);
    }

    public function createUser(Request $request)
    {
        // Validar los datos del formulario según tus necesidades
        $request->validate([
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'phone' => 'nullable|string|max:20',
            'username' => 'required|string|max:50|unique:users,username',
            'password' => 'required|string|max:50',
        ]);

        // Crear el usuario
        $usuario = User::create([
            'name' => $request->input('name'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
            'dateadd' => now(),
            'dateupdate' => now(),
        ]);
        if ($usuario) {
            return response()->json(['mensaje' => 'Usuario creado exitosamente', 'usuario' => $usuario]);
        } else{
            return response()->json(['mensaje' => 'Error de creando el usuario'], 401);
        }
    }

}